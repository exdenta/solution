﻿namespace CQG_SpellingChecker
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Input = new System.Windows.Forms.TextBox();
            this.Output = new System.Windows.Forms.TextBox();
            this.Check = new System.Windows.Forms.Button();
            this.Close = new System.Windows.Forms.Button();
            this.OpenFile = new System.Windows.Forms.Button();
            this.Clear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Input
            // 
            this.Input.Location = new System.Drawing.Point(12, 28);
            this.Input.Multiline = true;
            this.Input.Name = "Input";
            this.Input.Size = new System.Drawing.Size(195, 278);
            this.Input.TabIndex = 0;
            this.Input.Text = "rain spain plain plaint pain main mainly\r\nthe in on fall falls his was\r\n===\r\nhte " +
    "rame in pain fells\r\nmainy oon teh lain\r\nwas hints pliant\r\n===";
            this.Input.TextChanged += new System.EventHandler(this.Input_TextChanged);
            // 
            // Output
            // 
            this.Output.Location = new System.Drawing.Point(248, 32);
            this.Output.Multiline = true;
            this.Output.Name = "Output";
            this.Output.Size = new System.Drawing.Size(195, 278);
            this.Output.TabIndex = 1;
            // 
            // Check
            // 
            this.Check.Location = new System.Drawing.Point(495, 61);
            this.Check.Name = "Check";
            this.Check.Size = new System.Drawing.Size(112, 27);
            this.Check.TabIndex = 3;
            this.Check.Text = "Check";
            this.Check.UseVisualStyleBackColor = true;
            this.Check.Click += new System.EventHandler(this.Check_Click);
            // 
            // Close
            // 
            this.Close.Location = new System.Drawing.Point(495, 283);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(112, 23);
            this.Close.TabIndex = 4;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // OpenFile
            // 
            this.OpenFile.Location = new System.Drawing.Point(495, 28);
            this.OpenFile.Name = "OpenFile";
            this.OpenFile.Size = new System.Drawing.Size(112, 27);
            this.OpenFile.TabIndex = 2;
            this.OpenFile.Text = "Open File";
            this.OpenFile.UseVisualStyleBackColor = true;
            this.OpenFile.Click += new System.EventHandler(this.OpenFile_Click);
            // 
            // Clear
            // 
            this.Clear.Location = new System.Drawing.Point(495, 94);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(116, 23);
            this.Clear.TabIndex = 5;
            this.Clear.Text = "Clear";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 450);
            this.Controls.Add(this.Clear);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.Check);
            this.Controls.Add(this.OpenFile);
            this.Controls.Add(this.Output);
            this.Controls.Add(this.Input);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Input;
        private System.Windows.Forms.TextBox Output;
        private System.Windows.Forms.Button Check;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Button OpenFile;
        private System.Windows.Forms.Button Clear;
    }
}

