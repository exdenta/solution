﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQG_SpellingChecker
{
    class Dictionary
    {
        private List<string> words;

        public Dictionary(string text)
        {
            words = new List<string>();
            //Creating Dictionary
            string[] word=text.Split(new char[] { },StringSplitOptions.RemoveEmptyEntries);
            int i = 0;
            while (word[i] != "===")
            {
                string[] parts = word[i].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                for(int j=0;j<parts.Length;j++)
                    this.words.Add(parts[j]);
                i++;
            }
        }
        public string ReturnText()
        {
            string text=null;
            for (int i = 0; i < words.Count; i++)
                text += words[i] + " ";
            return text;
        }
        public List<string> GetList()
        {
            return words;
        }
    }
}
