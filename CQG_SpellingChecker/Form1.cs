﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CQG_SpellingChecker
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void OpenFile_Click(object sender, EventArgs e)
        {
            string fileName = null;

            using (OpenFileDialog openFileDialog1 = new OpenFileDialog())
            {
                openFileDialog1.InitialDirectory = "c:\\";
                openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog1.FilterIndex = 2;
                openFileDialog1.RestoreDirectory = true;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    fileName = openFileDialog1.FileName;
                }
            }

            if (fileName != null)
            {                
                Input.Text = File.ReadAllText(fileName);
            }
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Input_TextChanged(object sender, EventArgs e)
        {

        }

        private void Check_Click(object sender, EventArgs e)
        {
            Dictionary dictionary = new Dictionary(Input.Text);
            SpellChecker checker = new SpellChecker();
            Output.Text = checker.TextCheck(Input.Text, dictionary);
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            Input.Text = null;
            Output.Text = null;
        }
    }
}
