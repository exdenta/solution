﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQG_SpellingChecker
{
    class SpellChecker
    {
        string result=null;
        List<string> glossary;
        List<string[]> words;
        string[] alphabet = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z" };
        string[] alphabet2 = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "W", "X", "Y", "Z" };
        /// <summary>
        /// Checks if text was written correctly or not
        /// </summary>
        /// <param name="text"></param>
        /// <param name="dictionary"></param>
        /// <returns></returns>
        public string TextCheck(string text,Dictionary dictionary)
        {
            words = new List<string[]>();
            string [] line = text.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < line.Length; i++)
                words.Add(line[i].Split(new char[] { },StringSplitOptions.RemoveEmptyEntries)); 
            bool flag=false;
            glossary = new List<string>();
            glossary = dictionary.GetList();
            for (int i=0;i<words.Count;i++)
            {
                for (int j = 0; j < words[i].Length; j++)
                {

                    if (words[i][j] == "===" && flag)
                        flag = !flag;
                    if (flag)
                    {
                        string word = words[i][j];
                        word = word.ToLower();
                        //If in dictionary than just add to result
                        //Else check for Edits
                        if (FindCoincidence(word))
                        {
                            result += words[i][j] + " ";
                        }
                        else
                        {
                            List<String> semiresult = new List<string>();

                            semiresult = DeleteOneLetter(words[i][j], semiresult);

                            semiresult = AddOneLetter(words[i][j], semiresult);

                            semiresult = SwapLetters(words[i][j], semiresult);

                            semiresult = CheckSemiResult(semiresult);

                            if (semiresult.Count == 0)
                            {
                                semiresult = ChangeOneLetter(words[i][j], semiresult);
                                semiresult = AddTwoLetters(words[i][j], semiresult);
                                semiresult = DeleteTwoLetters(words[i][j], semiresult);
                            }

                            semiresult = CheckSemiResult(semiresult);

                            if (semiresult.Count > 1)
                            {
                                Print(semiresult);
                            }
                            if (semiresult.Count == 1)
                            {
                                result += semiresult[0] + " ";
                            }
                            if(semiresult.Count==0)
                            {
                                result += "{" + words[i][j] + "?} ";
                            }

                        }

                    }
                    if (words[i][j] == "===" && !flag)
                        flag = !flag;
                }
                if (flag)
                    result += Environment.NewLine;
            }
            return result;
        }
        /// <summary>
        /// Checks if there is a word when we swap 2 Letters
        /// </summary>
        /// <param name="word"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        private List<string> SwapLetters(string word, List<string> result)
        {
            for (int i = 0; i < word.Length-1; i++)
            {
                var sb = new StringBuilder();
                for (int j = 0; j < word.Length; j++)
                {
                    if (i != j && j != i + 1)
                    {
                        sb.Append(word[j]);
                    }
                    else
                        if (i == j)
                    {
                        sb.Append(word[j + 1]);
                        sb.Append(word[j]);
                    }
                }
                for(int k=0;k<glossary.Count;k++)
                {
                    if (sb.ToString() == glossary[k])
                        result.Add(sb.ToString());
                }
            }
                return result;
        }
        private void Print(List<string> semiresult)
        {
            result += "{ ";
            for (int k = 0; k < semiresult.Count; k++)
                result += semiresult[k] + " ";
            result += "?} ";
        }
        /// <summary>
        /// Checks if there is a word in dictionary or not
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        bool FindCoincidence(string word)
        {
            for (int i=0;i<glossary.Count;i++)
            {
                if (word == glossary[i])
                    return true;
            }
            return false;
        }
        /// <summary>
        /// Checks if there is a word without 1 letter in dictionary or not
        /// </summary>
        /// <param name="word"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        List<string> DeleteOneLetter(string word, List<string> result)
        {
            string r = word.ToLower();
            for (int j = 0; j < word.Length; j++)
            {
                string k = r.Remove(j, 1);
                for (int i = 0; i < glossary.Count; i++)
                {
                    if (k == glossary[i])
                    { 
                        r = word.Remove(j, 1);
                        result.Add(r);
                        r = word.ToLower();
                    }
                }
            }
            return result;
        }
        /// <summary>
        /// Checks if there is a word without 2 letters in dictionary or not
        /// </summary>
        /// <param name="word"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        List<string> DeleteTwoLetters(string word, List<string> result)
        {
            string r = word.ToLower();
            for (int j = 0; j < word.Length; j++)
            {
                string l = r.Remove(j, 1);
                for (int k = j; k < l.Length; k++)
                {
                    string res = l.Remove(k, 1);
                    for (int i = 0; i < glossary.Count; i++)
                    {
                        if (res == glossary[i])
                        {
                            r = word.Remove(j, 1);
                            r = r.Remove(k, 1);
                            result.Add(r);
                            r = word.ToLower();
                        }
                    }
                }
            }
            return result;
        }
        /// <summary>
        /// Checks if there is a word with 1 letter in dictionary or not
        /// </summary>
        /// <param name="word"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        List<string> AddOneLetter(string word, List<string> result)
        {
            string r = word.ToLower();
            for (int j = 0; j < word.Length; j++)
            {
                
                for (int alph = 0; alph < alphabet.Length; alph++)
                {
                    string k = r.Insert(j, alphabet[alph]);
                    for (int i = 0; i < glossary.Count; i++)
                    {
                        if (k == glossary[i])
                        {
                            if(j==0)
                                r = word.Insert(j, alphabet2[alph]);
                            else
                                r = word.Insert(j, alphabet[alph]);
                            result.Add(r);
                            r = word.ToLower();
                        }
                    }
                }
            }
            return result;
        }
        /// <summary>
        /// Checks if there is a word with 2 letters in dictionary or not
        /// </summary>
        /// <param name="word"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        List<string> AddTwoLetters(string word, List<string> result)
        {
            string low = word.ToLower();
            for (int j = 0; j < word.Length; j++)
            {

                for (int alph = 0; alph < alphabet.Length; alph++)
                {
                    string r = low.Insert(j, alphabet[alph]);
                    for (int k = 0; k < r.Length+1; k++)
                    {
                        for (int alph2 = 0; alph2 < alphabet.Length; alph2++)
                        {
                            string res = r.Insert(k, alphabet[alph2]);
                            for (int i = 0; i < glossary.Count; i++)
                            {
                                if (res == glossary[i])
                                {
                                    res = word.Insert(j, alphabet[alph]);
                                    res = res.Insert(k, alphabet[alph2]);
                                    result.Add(res);
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }
        List<string> ChangeOneLetter(string word, List<string> result)
        {
            for (int j = 0; j < word.Length; j++)
            {

                for (int alph = 0; alph < alphabet.Length; alph++)
                {                    
                    string r = word.Remove(j, 1);
                    r = r.Insert(j, alphabet[alph]);
                    for (int i = 0; i < glossary.Count; i++)
                    {
                        if (r == glossary[i])
                        {
                            if (Char.IsLower(word[j]))
                                result.Add(r);
                            else
                            {
                                r = word.Remove(j, 1);
                                r = r.Insert(j, alphabet2[alph]);
                                result.Add(r);
                            }
                        }
                    }
                }
            }
            return result;
        }
        /// <summary>
        /// Checks if there is a dublicates in List<>
        /// </summary>
        /// <param name="Checked"></param>
        /// <returns></returns>
        List<string> CheckSemiResult(List<string> Checked)
        {
            for (int i = 0; i < Checked.Count; i++)
                for (int j = i + 1; j < Checked.Count; j++)
                    if (Checked[i] == Checked[j])
                    {
                        Checked.Remove(Checked[i]);
                        j--;
                    }
            return Checked;
        }
    }
}
